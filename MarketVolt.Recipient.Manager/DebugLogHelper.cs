﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketVolt.Recipient.Manager
{
	public class DebugLogHelper
	{
		private string DebugHeader = "[{0} -- {1}] {2}";
		private List<string> logFile = new List<string>();
		private int _messageCode;
		private string _componentName;

		public DebugLogHelper(int code, string componentName)
		{
			_messageCode = code;
			_componentName = componentName;
		}

		public void LogEvent(string eventText)
		{
#if DEBUG
			var logEntry = string.Format(DebugHeader, DateTime.Now, _componentName + " " + _messageCode, eventText);
			Console.WriteLine(logEntry);
			logFile.Add(logEntry);
#endif
		}

		public void WriteFile() {
			try
			{
				System.IO.File.WriteAllLines(
					@"c:\code\logs\" + _componentName + _messageCode + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + @".txt", logFile);
			}
			catch (Exception exc2)
			{
				System.IO.File.WriteAllLines(@"c:\code\logs\" + _componentName + _messageCode + "_secondary_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + @".txt", logFile);
			}
		}

		public void LogError(Exception ex)
		{
			LogEvent("!!! ERROR !!!");
			LogEvent("Crash in progress, standby for log dump.");
			LogEvent("Incoming Stack Trace: ");
			LogEvent(ex.Source);
			LogEvent(ex.Message);
			LogEvent(ex.Data.ToString());
			LogEvent(ex.StackTrace);
			if (ex.InnerException != null)
			{
				LogEvent(Environment.NewLine);
				LogEvent("Logging InternalError");
				LogEvent(ex.InnerException.Source);
				LogEvent(ex.InnerException.Message);
				LogEvent(ex.InnerException.Data.ToString());
				LogEvent(ex.InnerException.StackTrace);
			}
			WriteFile();
		}
	}
}
