﻿using log4net;
using MarketVolt.ApiClient;
using MarketVolt.ApiClient.Model;
using MarketVolt.FileParser;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MarketVolt.Recipient.Manager {
	public class CSVProcessor {
		private IList<Tag> _tags = new List<Tag>();
		private RecipientClient _recipientClient;
		private TagClient _tagClient;
		private string _csvFilepath;
		private Regex emailRegex;

		private const string EMAIL_PATTERN = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
		                                     + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
		                                     + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

		private ILog _logger = LogManager.GetLogger("CSVProcessor");
		private DebugLogHelper logHelper = new DebugLogHelper(1, "CSVProcessor");

		public CSVProcessor(MVAccount account, string csvFile) {
			_tagClient = new TagClient(account);
			_recipientClient = new RecipientClient(account);
			_csvFilepath = csvFile;
		}


		public void ProcessTags() {
			if (_tags.Count == 0)
				_tags = GetAllExistingTags();

			emailRegex = new Regex(EMAIL_PATTERN, RegexOptions.IgnoreCase);
			var csvParser = new CSVFileParser(_csvFilepath);
			var rows = csvParser.ReadLine();
			bool firstRow = true;
			foreach (var row in rows) {
				if (firstRow) {
					var tagNameList = row.Row.Where(item => !emailRegex.IsMatch(item.Data)).Select(item => item.Header).ToList();
					foreach (var tagName in tagNameList) {
						if (!_tags.Any(item => item.Name.Equals(tagName, StringComparison.OrdinalIgnoreCase)))
							_tags.Add(CreateTag(tagName));
					}
					firstRow = false;
				}
				var csvData = row.Row.FirstOrDefault(item => emailRegex.IsMatch(item.Data));
				if (csvData == null) {
					_logger.Error("No properly formatted email found in the row");
					continue;
				}


				var selectedTagNames = row.Row.Where(item => !emailRegex.IsMatch(item.Data) && item.Data == "1")
					.Select(item => item.Header.ToLower()).ToList();
				var recipient = GetRecipient(csvData.Data);

				if (recipient == null) {
					_logger.ErrorFormat("Could not found recipient with {0}", csvData.Data);
					continue;
				}

				var selectedTagIds = _tags.Where(item => selectedTagNames.Contains(item.Name.ToLower()))
					.Select(item => item.Id).ToList();
				IList<string> errorMsgs;
				_recipientClient.ApplyTag(recipient.Id, selectedTagIds, out errorMsgs);

				if (errorMsgs != null || errorMsgs.Count > 0) {
					_logger.Error(errorMsgs);
				}
			}
		}

		public void ProcessUpload() {
			emailRegex = new Regex(EMAIL_PATTERN, RegexOptions.IgnoreCase);
			var csvParser = new CSVFileParser(_csvFilepath);
			var rows = csvParser.ReadLine();
			var batchtotal = 0;
			var batchSize = 20;
			var properties = typeof(ApiClient.Model.Recipient).GetProperties().ToList();
			var recipientList = new List<ApiClient.Model.Recipient>();
			foreach (var row in rows) {
				try {
					var addedRecipient = new ApiClient.Model.Recipient();
					foreach (var data in row.Row) {
						//Reflect to find if there's a Property that matches the current Header name.
						var targetProperty = properties.FirstOrDefault(x => x.Name.ToUpper() == data.Header.ToUpper());
						//Continue causes you to pop out to the next iteration of the current control statement.
						if (targetProperty == null) continue;
						var targetMethod = targetProperty.GetSetMethod();
						targetMethod.Invoke(addedRecipient, new object[] {data.Data});
					}
					StringBuilder builder = new StringBuilder();
					builder.Append("\n Recipient processed:");
					foreach (var property in addedRecipient.GetType().GetProperties().ToList()) {						
							builder.Append(Environment.NewLine + "\t" + property.Name + " : " + property.GetMethod.Invoke(addedRecipient, new object[] {}));
					}
					recipientList.Add(addedRecipient);
					logHelper.LogEvent(builder.ToString());
				}
				catch (Exception ex) {
					logHelper.LogError(ex);
				}
			}
			logHelper.WriteFile();
			IList<string> errors;
			var results = _recipientClient.CreateRecipients(recipientList, out errors);
		}

		private IList<Tag> GetAllExistingTags() {
			var result = new List<Tag>();
			var nextPage = 1;
			var totalPages = 1;
			do {
				var pagedResult = _tagClient.GetTags(new PageInfo {Page = nextPage, PageSize = 1000});
				totalPages = pagedResult.PageInfo.TotalPages;
				result.AddRange(pagedResult.Result);
			} while (nextPage < totalPages);
			return result;
		}

		private Tag CreateTag(string tagName) {
			var tag = new Tag {Name = tagName};
			tag.Id = _tagClient.CreateTag(tag);
			return tag;
		}

		private MarketVolt.ApiClient.Model.Recipient GetRecipient(string email) {
			var searchTerm = new SearchTerm {FieldName = "email", FieldValue = email, Operator = Operator.Equal};
			var response = _recipientClient.SearchRecipients(new Search {Terms = new List<SearchTerm> {searchTerm}});
			return response.Result != null ? response.Result.FirstOrDefault() : null;
		}
	}
}