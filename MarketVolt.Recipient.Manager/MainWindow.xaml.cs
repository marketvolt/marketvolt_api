﻿using System;
using MarketVolt.ApiClient;
using System.ComponentModel;
using System.Windows;

namespace MarketVolt.Recipient.Manager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BackgroundWorker worker; 
        public MainWindow()
        {
            InitializeComponent();
            worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
        }           

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = "*.csv";
            dlg.Filter = ".csv|*.csv";

            bool? hasFileSelected = dlg.ShowDialog();

            if (hasFileSelected == true)
                txtFileName.Text = dlg.FileName;
        }

        private void btnTagRecipients_Click(object sender, RoutedEventArgs e)
        {
            var account = StartWorking();
	        var arg = new WorkerArgurment
            {
                Account = account,
                FileLocation = txtFileName.Text,
				taskType = CsvTaskType.Tag
            };
            worker.RunWorkerAsync(arg);
        }

		private void btnUpload_Click(object sender, RoutedEventArgs e) {
			var account = StartWorking();
			var arg = new WorkerArgurment() {
				Account = account,
				FileLocation = txtFileName.Text,
				taskType = CsvTaskType.Upload
			};
			worker.RunWorkerAsync(arg);
		}

	    void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var passed = (WorkerArgurment)e.Argument;
            var processer = new CSVProcessor(passed.Account, passed.FileLocation);
	        switch (passed.taskType) {
		        case CsvTaskType.Tag:
					processer.ProcessTags();
			        break;
		        case CsvTaskType.Upload:
					processer.ProcessUpload();
			        break;
		        default:
			        throw new ArgumentOutOfRangeException();
	        }
	        
        }

	    void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
		    imgProgress.Visibility = System.Windows.Visibility.Hidden;
		    btnBrowse.IsEnabled = true;
		    btnUpload.IsEnabled = true;
		    btnTagRecipients.IsEnabled = true;
		    txtFileName.IsEnabled = true;

		    if (e.Error != null) {
			    MessageBox.Show(e.Error.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
		    }
	    }

		private MVAccount StartWorking()
		{
			imgProgress.Visibility = System.Windows.Visibility.Visible;
			btnBrowse.IsEnabled = false;
			btnUpload.IsEnabled = false;
			btnTagRecipients.IsEnabled = false;
			txtFileName.IsEnabled = false;
			var account = new MVAccount(txtAccount.Text, pwdKey.Password);
			return account;
		}

	    private class WorkerArgurment {
		    public MVAccount Account { get; set; }
		    public string FileLocation { get; set; }
		    public CsvTaskType taskType { get; set; }
	    }


    }

	internal enum CsvTaskType {
		Tag,
		Upload
	}
}
