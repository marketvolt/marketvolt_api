﻿using System;

namespace MarketVolt.ApiClient.Model
{
    public class Link
    {
        public string Id { get; set; }

        public string URL { get; set; }

        public string URLType { get; set; }

        public int Clicked { get; set; }

        public DateTime UtcScheduled { get; set; }

    }
}
