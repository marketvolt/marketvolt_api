﻿
namespace MarketVolt.ApiClient.Model
{
    public class Tag : ModelBase
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
