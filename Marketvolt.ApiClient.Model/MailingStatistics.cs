﻿using System;
using System.Collections.Generic;

namespace MarketVolt.ApiClient.Model
{
    public class MailingStatistics : ModelBase
    {
        public DateTime UtcScheduled { get; set; }

        public int Recipients { get; set; }

        private IList<Tag> _tags = new List<Tag>();      
        public IList<Tag> Tags
        {
            get { return _tags; }
            set { _tags = value; }
        }

        public int? Delivered { get; set; }

        public int? Bounced { get; set; }

        public int? Opened { get; set; }

        public int? RecipientWithClick { get; set; }

        public int? Forwarded { get; set; }

        public int? OptedOut { get; set; }

        private IList<Link> _clickedLinks = new List<Link>();

        public IList<Link> ClickedLinks
        {
            get { return _clickedLinks; }
            set { _clickedLinks = value; }
        }
    }
}
