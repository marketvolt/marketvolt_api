﻿using System;

namespace MarketVolt.ApiClient.Model
{
    public class Mailing : ModelBase
    {
        public string Name { get; set; }

        public DateTime UtcCreated { get; set; }
    }
}
