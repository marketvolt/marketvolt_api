﻿using System;
using System.Collections.Generic;

namespace MarketVolt.ApiClient.Model
{
    public class RecipientMailingStatistics : ModelBase
    {
        public string ExternalId { get; set; }

        public DateTime? UtcSent { get; set; }

        public DateTime? UtcOpened { get; set; }

        public int? ForwardedBy { get; set; }

        public string Status { get; set; }

        private IList<Link> _clickedLinks = new List<Link>();

        public IList<Link> ClickedLinks
        {
            get { return _clickedLinks; }
            set { _clickedLinks = value; }
        }

    }
}
