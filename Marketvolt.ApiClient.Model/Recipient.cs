﻿using System;
using System.Collections.Generic;

namespace MarketVolt.ApiClient.Model
{
    public class Recipient : ModelBase
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Status { get; set; }

        public DateTime? DateAdded { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Phone { get; set; }
        
        public DateTime? DateOfBirth { get; set; }

        //API sends and receives custom fields as key value list,  class KeyValuePair is not serialize-able
        //thus cannot be used as XML
        private IList<KeyValuePair<string, string>> _customFields = new List<KeyValuePair<string, string>>();

        public IList<KeyValuePair<string, string>> CustomFields
        {
            get { return _customFields; }
            set { _customFields = value; }
        }

        public DateTime? DateModified { get; set; }

        public DateTime? DateOptedIn { get; set; }

        public DateTime? DateOptedOut { get; set; }

        public string OptedOutReason { get; set; }

        public string ExternalId { get; set; }
    }
}
