﻿
namespace MarketVolt.ApiClient.Model
{
    /// <summary>
    /// Base class for all model objects
    /// </summary>
    public abstract class ModelBase
    {
        public int Id { get; set; }
        
        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
                return false;
            var item = (ModelBase)obj;
            return item.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
