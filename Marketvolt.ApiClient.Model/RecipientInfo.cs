﻿
namespace MarketVolt.ApiClient.Model
{
    public class RecipientInfo : ModelBase
    {
        public string ExternalId { get; set; }
    }
}
