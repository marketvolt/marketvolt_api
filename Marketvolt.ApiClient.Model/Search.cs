﻿using System.Collections.Generic;

namespace MarketVolt.ApiClient.Model
{
    public class Search
    {
        private IList<SearchTerm> _terms = new List<SearchTerm>();

        public IList<SearchTerm> Terms
        {
            get { return _terms; }
            set { _terms = value; }
        }

        private IList<SearchOrder> _orders = new List<SearchOrder>();

        public IList<SearchOrder> Orders
        {
            get { return _orders; }
            set { _orders = value; }
        }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }

    public class SearchTerm
    {
        public string FieldName { get; set; }

        public string FieldValue { get; set; }

        //This has to be string if we want to use XML serializer
        public Operator Operator { get; set; }

    }

    public class SearchOrder
    {
        public string FieldName { get; set; }

        //This has to be string if we want to use XML serializer
        public OrderBy OrderBy { get; set; }
    }

    public enum Operator
    {
        Equal,
        NotEqual,
        Greater,
        Less
    }

    public enum OrderBy
    {
        Asc,
        Desc
    }
}
