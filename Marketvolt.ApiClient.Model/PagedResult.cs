﻿using System.Collections.Generic;

namespace MarketVolt.ApiClient.Model
{
    public class PagedResult<T>
    {
        public PageInfo PageInfo { get; set; }

        public IList<T> Result { get; set; }
    }

    public class PageInfo
    {
        #region Fields
        public const int MAX_PAGE_SIZE = 1000;
        public const int MIN_PAGE_SIZE = 1;
        public const int DEFAULT_PAGE_SIZE = 100;

        private int _page = 1;
        private int _pageSize = DEFAULT_PAGE_SIZE;
        private int _totaPages = 1;
        #endregion

        public int TotalPages
        {
            get { return _totaPages; }
            set { _totaPages = value; }
        }

        public int Page
        {
            get { return _page; }
            set
            {
                _page = value;
                if (_page < 1)
                    _page = 1;
            }
        }

        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = value;
                if (_pageSize > MAX_PAGE_SIZE)
                    _pageSize = MAX_PAGE_SIZE;
                if (_pageSize < MIN_PAGE_SIZE)
                    _pageSize = DEFAULT_PAGE_SIZE;
            }
        }


       
    }
}
