﻿
using System;
namespace MarketVolt.ApiClient
{
    public class MVAccount
    {
        public MVAccount(string account,string key)
        {
            if (string.IsNullOrEmpty(account) || string.IsNullOrEmpty(key))
                throw new ArgumentException("Argument cannot be null or empty");

            AuthAccount = account;
            AuthKey = key;
        }
        
        public string AuthAccount { get; protected set; }

        public string AuthKey { get; protected set; }

        private string _urlAccount;
        public string UrlAccount
        {
            get
            {
                if (string.IsNullOrEmpty(_urlAccount))
                    return AuthAccount; 

                return _urlAccount;
            }
            set{ _urlAccount = value; }
        }
    }
}
