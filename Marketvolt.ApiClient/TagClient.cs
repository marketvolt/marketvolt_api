﻿
using MarketVolt.ApiClient.Model;
using MarketVolt.ApiClient.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarketVolt.ApiClient
{
    /// <summary>
    /// Wrapper class that executes Tag API calls
    /// </summary>
    public class TagClient
    {
        private MVAccount _accountInfo;

        public TagClient(MVAccount account)
        {
            _accountInfo = account;
        }

       /// <summary>
       /// Get Tags, paging info can be specified if needed
       /// </summary>
       /// <param name="pageInfo"></param>
       /// <returns></returns>
        public PagedResult<Tag> GetTags(PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/tags/{1}/{2}", _accountInfo.UrlAccount, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<Tag>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (Tag)item).ToList()
                };
            }
        }

        /// <summary>
        /// Get Tags for given recipient Id
        /// </summary>
        /// <param name="recipientId"></param>
        /// <returns></returns>
        public IList<Tag> GetTags(int recipientId)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/tags/recipient/{1}", _accountInfo.UrlAccount, recipientId));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return result.Data.Select(item => (Tag)item).ToList();
            }
        }

        
        /// <summary>
        /// Returns tag with given tag Id
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public Tag GetTag(int tagId)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/tag/{1}", _accountInfo.UrlAccount, tagId));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                if (result.Data == null || result.Data.Count == 0)
                    return null;

                return result.Data.Select(item => (Tag)item).FirstOrDefault();
            }
        }

        public int CreateTag(Tag tag)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult(Method.Put,
                            string.Format("{0}/tag", _accountInfo.UrlAccount),tag);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return result.Id;
            }
        }

        public int UpdateTag(Tag tag)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult(Method.Post,
                            string.Format("{0}/tag", _accountInfo.UrlAccount), tag);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return result.Id;
            }
        }

        public int DeleteTag(int tagId)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult(Method.Delete,
                            string.Format("{0}/tag/{1}", _accountInfo.UrlAccount,tagId));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return result.Id;
            }
        }
    }
}
