﻿using MarketVolt.ApiClient.Model;
using MarketVolt.ApiClient.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarketVolt.ApiClient
{
    /// <summary>
    /// Wrapper class that executes Recipient API calls
    /// </summary>
    public class RecipientClient
    {
        private MVAccount _accountInfo;

        public RecipientClient(MVAccount account)
        {
            _accountInfo = account;
        }

        #region Recipients
        /// <summary>
        /// Returns recipients, paging info can be provided if needed
        /// </summary>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public PagedResult<Recipient> GetRecipients(PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get, 
                            string.Format("{0}/recipients/{1}/{2}", _accountInfo.UrlAccount,pageInfo.Page,pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<Recipient>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize, TotalPages =  result.TotalPages  },
                    Result = result.Data.Select(item => (Recipient)item).ToList()
                };
            }
        }

        /// <summary>
        /// Returns recipient with given Id
        /// </summary>
        /// <param name="recipientId"></param>
        /// <returns></returns>
        public Recipient GetRecipient(int recipientId)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/recipient/{1}", _accountInfo.UrlAccount, recipientId));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                if (result.Data == null || result.Data.Count == 0) return null;

                return (Recipient)result.Data[0];
            }
        }

         public int CreateRecipient(Recipient recipient)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult<Recipient>(Method.Put,
                            string.Format("{0}/recipient", _accountInfo.UrlAccount),
                            recipient);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return result.Id;
            }
        }

        /// <summary>
        /// Update a recipient, This also creates recipient if it does not exist.
        /// </summary>
        /// <param name="recipient"></param>
        /// <returns></returns>
        public int CreateOrUpdateRecipient(Recipient recipient)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult<Recipient>(Method.Post,
                            string.Format("{0}/recipient", _accountInfo.UrlAccount),
                            recipient);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return result.Id;
            }
        }

        /// <summary>
        /// Batch creation of recipients
        /// Returns list successfully created recipient email with corresponding Id
        /// </summary>
        /// <param name="recipients"></param>
        /// <param name="errorMsgs"></param>
        /// <returns></returns>
        public IList<KeyValuePair<string, int>> CreateRecipients(IList<Recipient> recipients, out IList<string> errorMsgs)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (BatchResponse)client.GetResult(Method.BatchPut,
                            string.Format("{0}/recipients", _accountInfo.UrlAccount),
                            recipients);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                errorMsgs = new List<string>(result.Errors);

                return new List<KeyValuePair<string,int>>(result.ResponsedData);
            }
        }

        public int UpdateRecipient(Recipient recipient)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult(Method.Post,
                            string.Format("{0}/recipient", _accountInfo.UrlAccount),
                            recipient);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return result.Id;
            }
        }

        public void DeleteRecipient(int recipientId)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult(Method.Delete,
                            string.Format("{0}/recipient/{1}", _accountInfo.UrlAccount,recipientId));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);                
            }
        }

        /// <summary>
        /// Change the status of recipient with given Id
        /// Check API documentation for appropriate status names
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="status"></param>
        public void ChangeRecipientStatus(int recipientId, string status)
        {
            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult(Method.Post,
                            string.Format("{0}/recipient/{1}/status/{2}", _accountInfo.UrlAccount, recipientId,status));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);
            }
        }

        #endregion

        #region Recipients Tag

        /// <summary>
        /// Returns recipients for a given tag Id
        /// </summary>
        /// <param name="tagId"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public PagedResult<Recipient> GetRecipientsForTag(int tagId,PageInfo pageInfo = null)
        {
            if (tagId <= 0)
                throw new ArgumentException("Invalid tagId");
            
            if (pageInfo == null)
                pageInfo = new PageInfo();          

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/recipients/tag/{1}/{2}/{3}", _accountInfo.UrlAccount, tagId ,pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<Recipient>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (Recipient)item).ToList()
                };
            }
        }

        /// <summary>
        /// Apply tag to a recipient
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="tagId"></param>
        public void ApplyTag(int recipientId, int tagId)
        {
            if (recipientId <= 0)
                throw new ArgumentException("Invalid recipientId");
            
            if (tagId <= 0)
                throw new ArgumentException("Invalid tagId");

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult(Method.Post,
                            string.Format("{0}/recipient/{1}/tag/{2}", _accountInfo.UrlAccount, recipientId, tagId));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);                
            }
        }

        /// <summary>
        /// Remove tag from a recipient
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="tagId"></param>
        public void RemoveTag(int recipientId, int tagId)
        {
            if (recipientId <= 0)
                throw new ArgumentException("Invalid recipientId");

            if (tagId <= 0)
                throw new ArgumentException("Invalid tagId");

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (PostResponse)client.GetResult(Method.Delete,
                            string.Format("{0}/recipient/{1}/tag/{2}", _accountInfo.UrlAccount, recipientId, tagId));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);
            }
        }


        /// <summary>
        /// Apply multiple tags to a recipient
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="tagIds"></param>
        /// <param name="errorMsgs"></param>
        public void ApplyTag(int recipientId, IList<int> tagIds,out IList<string> errorMsgs)
        {
            if (recipientId <= 0)
                throw new ArgumentException("Invalid recipientId");

            if (tagIds.Count == 0)
                throw new ArgumentException("tagIds is empty");

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (BatchResponse)client.GetResult(Method.BatchPost,
                            string.Format("{0}/recipient/{1}/tags", _accountInfo.UrlAccount, recipientId), tagIds);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                errorMsgs = new List<string>(result.Errors);
            }
        }

        /// <summary>
        /// Remove multiple tags from a recipients
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="tagIds"></param>
        /// <param name="errorMsgs"></param>
        public void RemoveTag(int recipientId, IList<int> tagIds, out IList<string> errorMsgs)
        {
            if (recipientId <= 0)
                throw new ArgumentException("Invalid recipientId");

            if (tagIds.Count == 0)
                throw new ArgumentException("tagIds is empty");

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (BatchResponse)client.GetResult(Method.BatchDelete,
                            string.Format("{0}/recipient/{1}/tags", _accountInfo.UrlAccount, recipientId), tagIds);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                errorMsgs = new List<string>(result.Errors);
            }
        }

        /// <summary>
        /// Apply a tag to recipients
        /// </summary>
        /// <param name="recipientIds"></param>
        /// <param name="tagId"></param>
        /// <param name="errorMsgs"></param>
        public void ApplyTag(IList<int> recipientIds, int tagId, out IList<string> errorMsgs)
        {
            if (tagId <= 0)
                throw new ArgumentException("Invalid tagId");

            if (recipientIds.Count == 0)
                throw new ArgumentException("recipientIds is empty");

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (BatchResponse)client.GetResult(Method.BatchPost,
                            string.Format("{0}/recipients/tag/{1}", _accountInfo.UrlAccount, tagId), recipientIds);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                errorMsgs = new List<string>(result.Errors);
            }
        }

        /// <summary>
        /// Remove a tag from recipients
        /// </summary>
        /// <param name="recipientIds"></param>
        /// <param name="tagId"></param>
        /// <param name="errorMsgs"></param>
        public void RemoveTag(IList<int> recipientIds, int tagId, out IList<string> errorMsgs)
        {
            if (tagId <= 0)
                throw new ArgumentException("Invalid tagId");

            if (recipientIds.Count == 0)
                throw new ArgumentException("recipientIds is empty");

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (BatchResponse)client.GetResult(Method.BatchDelete,
                            string.Format("{0}/recipients/tag/{1}", _accountInfo.UrlAccount, tagId), recipientIds);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                errorMsgs = new List<string>(result.Errors);
            }
        }

        #endregion

        #region Recipient Search
        /// <summary>
        /// Returns list of recipients with given search conditions
        /// </summary>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public PagedResult<Recipient> SearchRecipients(Search search)
        {
            if (search == null)
                search = new Search();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Search,
                            string.Format("{0}/recipients", _accountInfo.UrlAccount),search);

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<Recipient>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (Recipient)item).ToList()
                };
            }
        }
        #endregion


    }
}
