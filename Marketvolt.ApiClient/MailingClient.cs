﻿using MarketVolt.ApiClient.Model;
using MarketVolt.ApiClient.Response;
using System;
using System.Linq;

namespace MarketVolt.ApiClient
{
    /// <summary>
    /// Wrapper class that executes Mailing API calls
    /// </summary>
    public class MailingClient
    {
        private MVAccount _accountInfo;

        public MailingClient(MVAccount account)
        {
            _accountInfo = account;
        }

        public PagedResult<Mailing> GetMailings(PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/{2}", _accountInfo.UrlAccount, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<Mailing>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (Mailing)item).ToList()
                };
            }
        }

        public PagedResult<MailingStatistics> GetStatistics(int mailingId, PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/statistics/{2}/{3}", _accountInfo.UrlAccount, mailingId, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<MailingStatistics>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (MailingStatistics)item).ToList()
                };
            }
        }

        public PagedResult<RecipientInfo> GetDelivered(int mailingId,PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/delivered/{2}/{3}", _accountInfo.UrlAccount, mailingId, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<RecipientInfo>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (RecipientInfo)item).ToList()
                };
            }
        }


        public PagedResult<RecipientInfo> GetBounced(int mailingId, PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/bounced/{2}/{3}", _accountInfo.UrlAccount, mailingId, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<RecipientInfo>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (RecipientInfo)item).ToList()
                };
            }
        }


        public PagedResult<RecipientInfo> GetOpened(int mailingId, PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/opened/{2}/{3}", _accountInfo.UrlAccount, mailingId, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<RecipientInfo>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (RecipientInfo)item).ToList()
                };
            }
        }

        public PagedResult<RecipientInfo> GetClicked(int mailingId, PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/clicked/{2}/{3}", _accountInfo.UrlAccount, mailingId, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<RecipientInfo>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (RecipientInfo)item).ToList()
                };
            }
        }


        public PagedResult<RecipientInfo> GetForwarded(int mailingId, PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/forwarded/{2}/{3}", _accountInfo.UrlAccount, mailingId, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<RecipientInfo>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (RecipientInfo)item).ToList()
                };
            }
        }

        public PagedResult<RecipientInfo> GetOptedOut(int mailingId, PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/optedout/{2}/{3}", _accountInfo.UrlAccount, mailingId, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<RecipientInfo>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (RecipientInfo)item).ToList()
                };
            }
        }

        public PagedResult<RecipientMailingStatistics> GetRecipientStatistic(int mailingId, int recipientId, PageInfo pageInfo = null)
        {
            if (pageInfo == null)
                pageInfo = new PageInfo();

            using (var client = new ApiHttpClient(_accountInfo))
            {
                var result = (GetResponse)client.GetResult(Method.Get,
                            string.Format("{0}/mailings/{1}/recipient/{2}/{3}/{4}", _accountInfo.UrlAccount, mailingId,
                            recipientId, pageInfo.Page, pageInfo.PageSize));

                if (!result.IsSuccess)
                    throw new Exception(result.Message);

                return new PagedResult<RecipientMailingStatistics>
                {
                    PageInfo = new PageInfo { Page = result.CurrentPage, PageSize = result.PageSize },
                    Result = result.Data.Select(item => (RecipientMailingStatistics)item).ToList()
                };
            }
        }
    }
}
