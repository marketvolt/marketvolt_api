﻿using MarketVolt.ApiClient.Model;
using MarketVolt.ApiClient.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace MarketVolt.ApiClient
{
    /// <summary>
    /// API returns different kinds of response POST and PUT.
    /// For example, POST batch action returns BatchResposne and in most case it returns PostResponse
    /// These enums are specific to API behaviors
    /// </summary>
    internal enum Method
    {
        Get,
        Post,
        Put,
        Delete,
        Search,
        BatchPost,
        BatchDelete,
        BatchPut
    }

    /// <summary>
    /// This class is helper to execute all restful API calls
    /// </summary>
    internal class ApiHttpClient : IDisposable
    {
        private HttpClient _client;       

        public ApiHttpClient(MVAccount account)
        {
            _client = new HttpClient();
            // Ignore untrusted ssl certificates
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErros) => true;
            _client.BaseAddress = GetBaseUrl();
            _client.DefaultRequestHeaders.Add("mvKey", account.AuthKey);
            _client.DefaultRequestHeaders.Add("mvaccount", account.AuthAccount);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));   
        }

        
        /// <summary>
        /// Executes URI that does not expect post body
        /// </summary>
        /// <param name="method"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        public ResponseBase GetResult(Method method, string uri)
        {
             return Execute<ModelBase>(method, uri, null);
        }


        /// <summary>
        /// Executes URI with post body
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="method"></param>
        /// <param name="uri"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ResponseBase GetResult<T>(Method method, string uri, T obj)
        {
            if (obj == null)
                throw new ArgumentException("obj cannot be null");

            return Execute<T>(method, uri, obj);
        }

        /// <summary>
        /// Executes URI for batch action
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="method"></param>
        /// <param name="uri"></param>
        /// <param name="objList"></param>
        /// <returns></returns>
        public ResponseBase GetResult<T>(Method method, string uri, IList<T> objList)
        {
            if (objList == null || objList.Count == 0)
                throw new ArgumentException("objList cannot be null or empty");

            return Execute<IList<T>>(method, uri, objList);
        }

        private ResponseBase Execute<T>(Method method, string uri, T postObject)
        {
            ResponseBase result;
            HttpResponseMessage response;
            //Json converter to read responses with list of objects (i.e. Tag or Recipient) 
            var converter = new MVJsonConverter();

            var formatter = new JsonMediaTypeFormatter();
            //API uses MicrosoftDateFormat in json
            formatter.SerializerSettings.DateFormatHandling = DateFormatHandling.MicrosoftDateFormat;
            formatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //There are some enum for ease of programming, API expects them as string
            formatter.SerializerSettings.Converters.Add(new StringEnumConverter());

            switch (method)
            {
                case Method.Delete:
                    response = _client.DeleteAsync(uri).Result;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<PostResponse>().Result;
                    break;

                case Method.Put:
                    response = _client.PutAsync(uri, postObject, formatter).Result;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<PostResponse>().Result;
                    break;

                //Some Post action does not need post body
                case Method.Post:
                    if (postObject != null)
                        response = _client.PostAsync(uri, postObject, formatter).Result;
                    else
                    {
                        var request = new HttpRequestMessage(HttpMethod.Post,uri);
                        response = _client.SendAsync(request,System.Threading.CancellationToken.None).Result;
                    }
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<PostResponse>().Result;
                    break;

                //Search is Post action but returns GetResponse
                case Method.Search:
                    response = _client.PostAsync(uri, postObject, formatter).Result;
                    response.EnsureSuccessStatusCode();
                    var jsonStr = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<GetResponse>(jsonStr, converter);
                    break;

                case Method.BatchPost:
                    response = _client.PostAsync(uri, postObject, formatter).Result;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<BatchResponse>().Result;
                    break;

                case Method.BatchDelete:
                    var  deleteMessage = new HttpRequestMessage(HttpMethod.Delete,uri);
                    deleteMessage.Content = new StringContent(JsonConvert.SerializeObject(postObject), System.Text.Encoding.UTF8, "application/json");
                    response = _client.SendAsync(deleteMessage,System.Threading.CancellationToken.None).Result;;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<BatchResponse>().Result;
                    break;

                case Method.BatchPut:
                    response = _client.PutAsync(uri, postObject, formatter).Result;
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<BatchResponse>().Result;
                    break;

                case Method.Get:
                default:
                    response = _client.GetAsync(uri).Result;
                    response.EnsureSuccessStatusCode();
                    jsonStr = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<GetResponse>(jsonStr, converter);
                    break;
            }

            return result;

        }

        public void Dispose()
        {
            if (_client == null)
                return;

            _client.Dispose();
        }

        /// <summary>
        /// Reads API base URL from app config
        /// </summary>
        /// <returns></returns>
        private Uri GetBaseUrl()
        {
            var settings = ConfigurationManager.AppSettings;
            
            if (settings.Count == 0)
                throw new Exception("mvUrl is not configured in App Settings");

            string url = settings["mvUrl"];

            if (string.IsNullOrEmpty(url))
                throw new Exception("mvUrl is not configured in App Settings");

            Uri uriResult;
            bool isValidUri = Uri.TryCreate(url, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
            if (!isValidUri)
                throw new Exception("Improper url or not Https scheme");

            return uriResult;
        }
    }
}
