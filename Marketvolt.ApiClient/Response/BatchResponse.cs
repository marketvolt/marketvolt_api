﻿using System.Collections.Generic;

namespace MarketVolt.ApiClient.Response
{
    public class BatchResponse : ResponseBase
    {
        public IList<string> Errors { get; set; }

        public IList<KeyValuePair<string, int>> ResponsedData { get; set; }
    }
}
