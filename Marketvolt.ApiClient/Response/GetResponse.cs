﻿using MarketVolt.ApiClient.Model;
using System.Collections.Generic;

namespace MarketVolt.ApiClient.Response
{
    public class GetResponse : ResponseBase
    {
        public int TotalPages { get; set; }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }

        public IList<ModelBase> Data { get; set; }
    }
}
