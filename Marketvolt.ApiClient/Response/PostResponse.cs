﻿
namespace MarketVolt.ApiClient.Response
{
    public class PostResponse : ResponseBase
    {
        public string Type { get; set; }

        public int Id { get; set; }
    }
}
