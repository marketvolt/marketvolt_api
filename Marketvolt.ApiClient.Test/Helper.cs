﻿
using System;
using System.Text;
namespace MarketVolt.ApiClient.Test
{
    public static class Helper
    {
        private static Random _random = new Random((int)DateTime.Now.Ticks);  

        public static MVAccount GetAccount()
        {
            return new MVAccount("put account number here", "put api key here");
        }

        public static string GetRandomString(int size = 5)
        {
            if(size < 1)
                throw new ArgumentException("size cannot be less than 1");

            var builder = new StringBuilder();
            for (int i = 0; i < size; i++)
                builder.Append(Convert.ToChar(Convert.ToInt32(Math.Floor(26* _random.NextDouble() + 65))));
            return builder.ToString();
        }

        public static int GetRandomNum(int min = 10, int max = 99)
        {
           return _random.Next(min, max);
        }
    }
}
