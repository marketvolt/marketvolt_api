﻿using MarketVolt.ApiClient.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarketVolt.ApiClient.Test
{
    /// <summary>
    /// This unit is not complete on its own, it requires valid Restful Url and account information.
    /// In case of search and custom fields, it has only visual validation based on web UI
    /// </summary>
    [TestFixture]
    public class ApiTest
    {

        private RecipientClient _recipientClient = new RecipientClient(Helper.GetAccount());
        private TagClient _tagClient = new TagClient(Helper.GetAccount());

        [Test]
        public void CreateRecipient()
        {
            var recipient = SetupRecipient();
            
            //Empty email
            recipient.Email = string.Empty;
            Assert.Throws<Exception>( () => _recipientClient.CreateRecipient(recipient));

            //improper email format
            recipient.Email = string.Format("{0}cc@home.com]",Helper.GetRandomString());
            Assert.Throws<Exception>(() => _recipientClient.CreateRecipient(recipient));

            //improper email format
            recipient.Email = string.Format("{0}cc@homecom", Helper.GetRandomString());
            Assert.Throws<Exception>(() => _recipientClient.CreateRecipient(recipient));

            recipient.Email = string.Format("{0}cc@home.com", Helper.GetRandomString());
            //custom field data not in proper format
            recipient.CustomFields[1] = new KeyValuePair<string, string>("TestTwo", "");
            Assert.Throws<Exception>(() => _recipientClient.CreateRecipient(recipient));

            recipient.CustomFields[1] = new KeyValuePair<string, string>("TestTwo", DateTime.UtcNow.ToString("d"));

            recipient.Id = 2321434; // to show Id is always is assigned
            var theId = _recipientClient.CreateRecipient(recipient);
            Assert.AreNotEqual(theId, recipient.Id);
            //duplicate email 
            Assert.Throws<Exception>(() => _recipientClient.CreateRecipient(recipient));

            _recipientClient.DeleteRecipient(theId);

            //once recipient is deleted, recipient can be created with same email
            var newId = _recipientClient.CreateRecipient(recipient);

            Assert.AreNotEqual(theId, newId);
        }

        [Test]
        public void UpdateRecipient()
        {
            var recipient = SetupRecipient();

            //calling update email with new recipient should create one
            recipient.Id = _recipientClient.UpdateRecipient(recipient);
            Assert.AreNotEqual(0, recipient.Id);

            recipient.LastName = "modifiedName";
            recipient.Status = "Pending";
            recipient.DateOptedIn = DateTime.Now;
            recipient.DateAdded = DateTime.Now.AddYears(5);
            recipient.DateOptedOut = DateTime.Now;
           
            _recipientClient.UpdateRecipient(recipient);

            var retRecipient = _recipientClient.GetRecipient(recipient.Id);

            Assert.AreEqual(recipient.LastName, retRecipient.LastName);
            //status does not get modified
            Assert.AreEqual("Active", retRecipient.Status);
            //read only properties does not get updated
            Assert.AreNotEqual(recipient.DateAdded.Value.Year, retRecipient.DateAdded.Value.Year);
            Assert.IsFalse(retRecipient.DateOptedIn.HasValue);
            Assert.IsFalse(retRecipient.DateOptedOut.HasValue);

            recipient = SetupRecipient();
            recipient.Id = _recipientClient.CreateRecipient(recipient);
            recipient.Email = retRecipient.Email;
            //updating recipient with existing email 
            Assert.Throws<Exception>(() => _recipientClient.UpdateRecipient(recipient));            

        }

        [Test]
        public void DeleteRecipient()
        {
            var recipient01 = SetupRecipient();
            recipient01.Id = _recipientClient.CreateRecipient(recipient01);
            var recipient02 = SetupRecipient();
            recipient02.Id = _recipientClient.CreateRecipient(recipient02);
            var recipient03 = SetupRecipient();
            recipient03.Id = _recipientClient.CreateRecipient(recipient03);

            _recipientClient.ChangeRecipientStatus(recipient01.Id,"Invalid Email");
            _recipientClient.ChangeRecipientStatus(recipient02.Id, "Opted-Out");
            //the recipient with invalid email cannot be deleted
            Assert.Throws<Exception>(() => _recipientClient.DeleteRecipient(recipient01.Id));

            //the recipient with opted-out email cannot be deleted
            Assert.Throws<Exception>(() => _recipientClient.DeleteRecipient(recipient02.Id));

            //the recipient with with bad Id
            Assert.Throws<Exception>(() => _recipientClient.DeleteRecipient(0));

            _recipientClient.DeleteRecipient(recipient03.Id);

            var retRecipient = _recipientClient.GetRecipient(recipient03.Id);

            Assert.IsNull(retRecipient);
        }

        [Test]
        public void UpdateStatus()
        {
            var recipient01 = SetupRecipient();
            recipient01.Id = _recipientClient.CreateRecipient(recipient01);
            var recipient02 = SetupRecipient();
            recipient02.Id = _recipientClient.CreateRecipient(recipient02);
            var recipient03 = SetupRecipient();
            recipient03.Id = _recipientClient.CreateRecipient(recipient03);

            _recipientClient.ChangeRecipientStatus(recipient01.Id, "Invalid Email");
            _recipientClient.ChangeRecipientStatus(recipient02.Id, "Opted-Out");

            Assert.Throws<Exception>(() => _recipientClient.ChangeRecipientStatus(recipient01.Id, "Pending"));

            Assert.Throws<Exception>(() => _recipientClient.ChangeRecipientStatus(recipient02.Id, "Active"));

            Assert.Throws<Exception>(() => _recipientClient.ChangeRecipientStatus(recipient03.Id, "InvalidStatus"));

            _recipientClient.ChangeRecipientStatus(recipient03.Id, "Suspended");

            Assert.Throws<Exception>(() => _recipientClient.ChangeRecipientStatus(1, "Pending"));
           
        }

        [Test]
        public void GetRecipients()
        {
            var result = _recipientClient.GetRecipients();
            Assert.AreEqual(result.PageInfo.Page, 1);
            Assert.AreEqual(result.PageInfo.PageSize, 100);

            result = _recipientClient.GetRecipients(new PageInfo { Page = 2, PageSize = 1000 });

            Assert.AreEqual(result.PageInfo.Page, 2);
            Assert.AreEqual(result.PageInfo.PageSize, 1000);

            result = _recipientClient.GetRecipients(new PageInfo { Page = result.PageInfo.TotalPages + 1 , PageSize = 1000 });
            Assert.Greater(result.PageInfo.Page, result.PageInfo.TotalPages);
            Assert.AreEqual(result.PageInfo.PageSize, 1000);
            Assert.IsEmpty(result.Result);

            result = _recipientClient.GetRecipients(new PageInfo { Page = 0, PageSize = 1000 });
            Assert.AreEqual(result.PageInfo.Page, 1);
            Assert.AreEqual(result.Result.Count,1000);

            result = _recipientClient.GetRecipients(new PageInfo { Page = 1, PageSize = 2000 });
            Assert.AreEqual(result.PageInfo.Page, 1);
            Assert.AreEqual(result.Result.Count, 1000);
        }

        [Test]
        public void SearchRecipients()
        {
            var search = new Search();
            search.Terms.Add(new SearchTerm { FieldName = "lastName", FieldValue = "Example", Operator = Operator.Equal });
            search.Terms.Add(new SearchTerm { FieldName = "lastName", FieldValue = "Test", Operator = Operator.Equal });
            search.Terms.Add(new SearchTerm { FieldName = "dateAdded", FieldValue = "03/01/2014", Operator = Operator.Greater });
            search.Orders.Add(new SearchOrder { FieldName = "firstName", OrderBy = OrderBy.Desc });
            var result = _recipientClient.SearchRecipients(search);
            Assert.IsNotEmpty(result.Result);
            search.Terms.Add(new SearchTerm { FieldName = "dateOfBirth", FieldValue = "null", Operator = Operator.Equal });
            result = _recipientClient.SearchRecipients(search);
            Assert.IsNotEmpty(result.Result);

            search.Terms.Clear();
            search.Terms.Add(new SearchTerm { FieldName = "lastName", FieldValue = "", Operator = Operator.Equal });
            Assert.Throws<Exception>(() => _recipientClient.SearchRecipients(search));

            search.Terms.Clear();
            search.Terms.Add(new SearchTerm { FieldName = "somethingElse", FieldValue = "some", Operator = Operator.Equal });
            Assert.Throws<Exception>(() => _recipientClient.SearchRecipients(search));

            search.Terms.Clear();
            search.Terms.Add(new SearchTerm { FieldName = "", FieldValue = "some", Operator = Operator.Equal });
            Assert.Throws<Exception>(() => _recipientClient.SearchRecipients(search));

            //searching custom field
            search.Terms.Clear();
            search.Terms.Add(new SearchTerm { FieldName = "lastName", FieldValue = "modifiedName", Operator = Operator.Equal });
            search.Terms.Add(new SearchTerm { FieldName = "testTwo", FieldValue = "5/6/2014", Operator = Operator.Equal });            
            result = _recipientClient.SearchRecipients(search);
            Assert.IsNotEmpty(result.Result);

            search.Terms.Clear();
            search.Terms.Add(new SearchTerm { FieldName = "testTwo", FieldValue = "5/6/2014", Operator = Operator.Greater });
            Assert.Throws<Exception>(() => _recipientClient.SearchRecipients(search));
        }

        [Test]
        public void ApplyRemoveTags()
        {
            var recipientIds = _recipientClient.GetRecipients(new PageInfo{Page=1, PageSize =1000})
                                .Result.Select( item => item.Id).ToList();

            var tags = _tagClient.GetTags().Result;

            var tag = SetupTag();
            tag.Id = _tagClient.CreateTag(tag);

            var repId = recipientIds[Helper.GetRandomNum(1, 99)];

            //passing wrong tag Id
            Assert.Throws<Exception>(() => _recipientClient.ApplyTag(repId, 1));

            //passing wrong recipient Id
            Assert.Throws<Exception>(() => _recipientClient.ApplyTag(1, tag.Id));

            _recipientClient.ApplyTag(repId, tag.Id);
            
            //removing tag from a invalid recipient Id
            Assert.Throws<Exception>( () => _recipientClient.RemoveTag(recipientIds[Helper.GetRandomNum(100, recipientIds.Count-2)], tag.Id));

            //removing invalid tag 
            Assert.Throws<Exception>(() => _recipientClient.RemoveTag(repId, 1));

            //removing default tag 
            var tagId = tags.FirstOrDefault(tg => tg.Name.Contains("Default")).Id;
            Assert.Throws<Exception>(() => _recipientClient.RemoveTag(repId, tagId));

            //reapplying same tag
            _recipientClient.ApplyTag(repId, tag.Id);

            IList<string> messages;
            var dummyIds =Enumerable.Range(1, 10).ToList();
            dummyIds.Add(recipientIds[Helper.GetRandomNum(100, recipientIds.Count - 2)]);
            _recipientClient.ApplyTag(dummyIds, tag.Id, out messages);
            //all dummies recipients
            Assert.AreEqual(messages.Count, 10);

            _recipientClient.ApplyTag(recipientIds, tag.Id, out messages);
            //two recipients were already applied
            Assert.AreEqual(messages.Count, 2);

            Assert.Throws<Exception>(() => _recipientClient.ApplyTag(recipientIds, 1,out messages));

            Assert.Throws<Exception>(() => _recipientClient.RemoveTag(recipientIds, 1, out messages));

            //removing default tag
            Assert.Throws<Exception>(() => _recipientClient.RemoveTag(recipientIds, tagId, out messages));

            _recipientClient.RemoveTag(repId, tag.Id);

            _recipientClient.RemoveTag(recipientIds, tag.Id, out messages);

            Assert.AreEqual(messages.Count, 1);

            var tagids = tags.Select(tg => tg.Id).ToList();

            _recipientClient.ApplyTag(repId, tagids, out messages);
            Assert.AreEqual(messages.Count, 1);
            
            _recipientClient.RemoveTag(repId, tagids, out messages);

            Assert.GreaterOrEqual(messages.Count, 1);

            _tagClient.DeleteTag(tag.Id);
        }

        [Test]
        public void CreateUpdateDeleteGetTag()
        {
            var tag = SetupTag();
            tag.Id = _tagClient.CreateTag(tag);

            tag.Name = "modifiedTagName";

            _tagClient.UpdateTag(tag);

            var retTag = _tagClient.GetTag(tag.Id);

            Assert.AreEqual(tag.Name, retTag.Name);

            _tagClient.DeleteTag(tag.Id);

            Assert.Throws<Exception>(() => _tagClient.GetTag(tag.Id));

            Assert.Throws<Exception>(() => _tagClient.GetTag(1));

            Assert.Throws<Exception>(() => _tagClient.GetTags(1));

            var rep = _recipientClient.GetRecipients().Result.FirstOrDefault();

            var tags = _tagClient.GetTags(rep.Id);
            Assert.IsNotEmpty(tags);
        }

       

        private Recipient SetupRecipient()
        {
            var recipient = new Recipient
            {
                Email = string.Format("{0}@doamin.com", Helper.GetRandomString()),
                FirstName = Helper.GetRandomString(),
                LastName = Helper.GetRandomString(),                
                DateOfBirth = DateTime.Now.AddYears(-Helper.GetRandomNum())
            };
            recipient.CustomFields.Add(new KeyValuePair<string, string>("TestOne", Helper.GetRandomString()));
            recipient.CustomFields.Add(new KeyValuePair<string, string>("TestTwo", DateTime.UtcNow.ToString("d")));
            return recipient;
        }

        private Tag SetupTag()
        {
            return new Tag
            {
                Name = Helper.GetRandomString(),
                Description = Helper.GetRandomString(20)
            };
        }        


    }
}
