# README #

This is a C#/Visual Studio 2012 Solution with projects containing a MarketVolt API Example program and a MarketVolt API Wrapper Library.

[MarketVolt API Documentation](https://marketvolt.atlassian.net/wiki/display/MH/MarketVolt+API+and+Webhooks)

[Raw API Endpoint Reference](https://api.marketvolt.com/v1/help)

### How do I get set up? ###

You must do the following first:

1. Have access to an active MarketVolt Account
1. The account must be API enabled.  Contact MarketVolt Support.
1. Know your MarketVolt Account #.
1. Have an API key for a MarketVolt user with administrative privileges.
1. Modify ApiExample.cs and enter your account number and api key.
1. Modify ApiClient.Test.Helper.cs and enter your account number and api key.


### Contribution guidelines ###

Contact Developers@marketvolt.com

### Who do I talk to? ###

Contact support@marketvolt.com