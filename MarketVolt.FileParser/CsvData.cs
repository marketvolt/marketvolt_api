﻿
namespace MarketVolt.FileParser
{
    public class CsvData
    {
        public string Header { get; set; }

        public string Data {get; set;}       
    }
}