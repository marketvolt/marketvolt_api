﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MarketVolt.FileParser
{
    /// <summary>
    /// This Parser assumes first line is always a header
    /// </summary>
    public class CSVFileParser
    {
        private string _filePath;
        private IList<string> _headers = new List<string>();
        
        public CSVFileParser(string filePath)
        {
            if (!File.Exists(filePath))
                throw new Exception("Invalid path");
           _filePath = filePath;
        }

        public IList<string> Headers
        {
            get { return _headers; }
        }
        
        public IEnumerable<CsvDataRow> ReadLine()
        {
            using(var streamReader = new StreamReader(_filePath))
            {
                string line = null;               
                while ((line = streamReader.ReadLine()) != null)
                {
                    var individualValues = line.Split(',').ToList();
                    if (_headers.Count == 0)
                    {
                        _headers = individualValues;
                        continue;
                    }
                    
                    yield return new CsvDataRow
                    {
                        Row = _headers.Zip(individualValues, (first, second) => new CsvData { Header = first, Data = second }).ToList()
                    };
                }
            }
            
        }
    }
}
