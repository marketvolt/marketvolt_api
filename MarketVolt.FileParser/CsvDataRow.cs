using System.Collections.Generic;

namespace MarketVolt.FileParser
{
    public class CsvDataRow
    {
        private IList<CsvData> _row = new List<CsvData>();

        public IList<CsvData> Row
        {
            get { return _row; }
            set { _row = value; }
        }
    }
}
