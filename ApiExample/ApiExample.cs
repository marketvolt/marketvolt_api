﻿
using MarketVolt.ApiClient.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarketVolt.ApiClient
{
    public class ApiExample
    {
        public static void Main(string[] args)
        {
            //Need to get account info from Marketvolt administration
			var MyMVAccountID = "0"; //  Must set this.
			var MyMVAPIKey = "00000000-0000-0000-0000-000000000000";  // Must set this.  Get api key from MVApp User Profile page.
			var account = new MVAccount(MyMVAccountID, MyMVAPIKey);
            
            var recipientClient = new RecipientClient(account);
            var tagClient = new TagClient(account);
            try
            {

                Console.WriteLine("Executing GetRecipients");
                var repResult = recipientClient.GetRecipients();
                Console.WriteLine("Current Page {0} Recipients Count {1} Total Page {2}", 
                    repResult.PageInfo.Page, repResult.Result.Count, repResult.PageInfo.TotalPages);
                Console.WriteLine();

                Console.WriteLine("Executing SearchRecipients");
                var search = new Search();
                search.Terms.Add(new SearchTerm{ FieldName="lastName", FieldValue="Example", Operator = Operator.Equal});
                search.Orders.Add(new SearchOrder { FieldName = "firstName", OrderBy = OrderBy.Desc });
                repResult = recipientClient.SearchRecipients(search);
                foreach (var item in repResult.Result)
                    Console.WriteLine(" Recipient Email {0}  FirstName {1} LastName {2}", item.Email, item.FirstName, item.LastName);
                Console.WriteLine();

                Console.WriteLine("Executing CreateRecipient");
                var recipient = SetupRecipient();
                recipient.Id = recipientClient.CreateRecipient(recipient);
                Console.Write("Recipient with Id {0} created", recipient.Id);
                Console.WriteLine("{0}", Environment.NewLine);

                Console.WriteLine("Executing CreateOrUpdateRecipient");
                recipient.Zip = "2002"; 
                recipient.Id = recipientClient.CreateOrUpdateRecipient(recipient);
                Console.Write("Recipient with Id {0} updated", recipient.Id);
                Console.WriteLine("{0}", Environment.NewLine);
                

                Console.WriteLine("Executing CreateRecipients");
                var recipientList = SetupRecipientList();
                IList<string> errorMsgs;
                var batchCreatedRecipients = recipientClient.CreateRecipients(recipientList, out errorMsgs);
                Console.WriteLine("Total Error Messages {0}", errorMsgs.Count);
                foreach (var error in errorMsgs)
                    Console.WriteLine(error);
                Console.WriteLine("{0} {1}", Environment.NewLine, Environment.NewLine);

                Console.WriteLine("Executing GetTags");
                var tagResult = tagClient.GetTags();
                foreach (var item in tagResult.Result)
                    Console.WriteLine("Name {0}", item.Name);
                Console.WriteLine();

                Console.WriteLine("Executing CreateTag");
                var tag = SetupTag();
                tag.Id = tagClient.CreateTag(tag);
                Console.Write("Tag with Id {0} created", tag.Id);
                Console.WriteLine("{0}", Environment.NewLine);

                Console.WriteLine("Executing ApplyTag");
                recipientClient.ApplyTag(recipient.Id, tag.Id);

                Console.WriteLine("Executing GetTags for recipient {0}", recipient.Id);
                var recipientTags = tagClient.GetTags(recipient.Id);
                foreach (var item in recipientTags)
                    Console.WriteLine("Name {0}", item.Name);
                Console.WriteLine("{0}", Environment.NewLine);

                Console.WriteLine("Executing RemoveTag");
                recipientClient.RemoveTag(recipient.Id, tag.Id);

                Console.WriteLine("Executing GetTags for recipient {0}", recipient.Id);
                recipientTags = tagClient.GetTags(recipient.Id);
                foreach (var item in recipientTags)
                    Console.WriteLine("Name {0}", item.Name);

                Console.WriteLine("{0}", Environment.NewLine);

                Console.WriteLine("Executing Apply Multiple Tags to a recipient", recipient.Id);
                var tagIds = tagResult.Result.Select( item => item.Id).ToList();
                recipientClient.ApplyTag(recipient.Id, tagIds,out errorMsgs);

                Console.WriteLine("{0}", Environment.NewLine);

                Console.WriteLine("Executing Remove Multiple Tags from a recipient", recipient.Id);
                recipientClient.RemoveTag(recipient.Id, tagIds, out errorMsgs);

                Console.WriteLine("Executing GetTags for recipient {0}", recipient.Id);
                recipientTags = tagClient.GetTags(recipient.Id);
                foreach (var item in recipientTags)
                    Console.WriteLine("Name {0}", item.Name);

                Console.WriteLine("{0}", Environment.NewLine);

                Console.WriteLine("Executing Apply Tags to multiple recipients");
                var recipientIds = repResult.Result.Select(x => x.Id).ToList();
                recipientClient.ApplyTag(recipientIds,tag.Id,out errorMsgs);

                Console.WriteLine("Executing Remove Tags form multiple recipients");
                recipientClient.RemoveTag(recipientIds, tag.Id, out errorMsgs);

                Console.WriteLine("{0}", Environment.NewLine);

                Console.WriteLine("Executing Get a tag");                
                tagClient.GetTag(tag.Id);

                Console.WriteLine("Executing Get a recipient");
                recipientClient.GetRecipient(recipient.Id);

                Console.WriteLine("Delete Recipient");               
                recipientClient.DeleteRecipient(recipient.Id);

                Console.WriteLine("Delete Tag");
                tagClient.DeleteTag(tag.Id);   


            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }

            Console.ReadKey(true);
        }

        private static IList<Recipient> SetupRecipientList(int count = 1000)
        {
            var list = new List<Recipient>();
            for (int i = 0; i < count; i++)
                list.Add(SetupRecipient());
            return list;
        }

        private static Recipient SetupRecipient()
        {
            System.Threading.Thread.Sleep(10);
            var ticks = DateTime.Now.Ticks;
            var recipient = new Recipient()
            {
                Email = string.Format("therecipient{0}@email.com", ticks),
                FirstName = string.Format("The first {0}", ticks),
                LastName = string.Format("The last {0}", ticks),
                Address = "the address",
                City = "the city",
                State = "MO",
                Zip = "1001",
                DateOfBirth = DateTime.UtcNow.AddYears(-20)               
            };
            //These two custom field was created for the account in UI
            recipient.CustomFields.Add(new KeyValuePair<string, string>("TestOne", string.Format("FirstOne {0}", ticks))); 
            recipient.CustomFields.Add(new KeyValuePair<string, string>("TestTwo", DateTime.UtcNow.ToString("d")));
            return recipient;
        }

        private static Tag SetupTag()
        {
             var ticks = DateTime.Now.Ticks;
             return new Tag
             {
                 Name = string.Format("Tag{0}", ticks),
                 Description = "the description of the tag"
             };
        }


    }
}
